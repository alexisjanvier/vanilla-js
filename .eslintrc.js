module.exports = {
	env: {
		browser: true,
		es2021: true,
	},
	plugins: ["compat"],
	extends: ["eslint:recommended", "plugin:compat/recommended", "prettier"],
	parserOptions: {
		ecmaVersion: "latest",
		sourceType: "module",
	},
	rules: {},
};
