dev:
  static-web-server --port 8080 --log-level info --grace-period 1 --root ./src
start:
  npm run dev
lint:
  ./rome format --quote-style=single --semicolons=always src --write
