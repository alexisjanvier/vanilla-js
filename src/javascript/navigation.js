import { generateBookList, generateMusicList } from './list-manager.js';
import { renderHome } from './island-beach.js';

const homeLink = document.querySelector('a[href="/"]');
const musicLink = document.querySelector('a[href="/disques"]');
const booksLink = document.querySelector('a[href="/livres"]');

export const initNavigation = () => {
    homeLink.addEventListener('click', goToHome);
    musicLink.addEventListener('click', goToMusic);
    booksLink.addEventListener('click', goToBooks);
};

export const goToMusic = async (event) => {
    event.stopPropagation();
    event.preventDefault();
    homeLink.removeAttribute('role');
    booksLink.removeAttribute('role');
    musicLink.setAttribute('role', 'button');
    await generateMusicList();
    return false;
};

export const goToBooks = async (event) => {
    event.stopPropagation();
    event.preventDefault();
    homeLink.removeAttribute('role');
    musicLink.removeAttribute('role');
    booksLink.setAttribute('role', 'button');
    await generateBookList();
    return false;
};

export const goToHome = async (event) => {
    event.stopPropagation();
    event.preventDefault();
    booksLink.removeAttribute('role');
    musicLink.removeAttribute('role');
    homeLink.setAttribute('role', 'button');
    await renderHome();
    return false;
};
