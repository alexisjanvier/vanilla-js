export const generateMusicList = async () => {
    const container = document.querySelector('main');
    const apiResponse = await fetch('/data/albums.json');
    const albumsInJson = await apiResponse.json();
    const musicList = document.createElement('tips-list');
    musicList.tips = albumsInJson.albums;
    container.innerHTML = '';
    container.appendChild(musicList);
};

export const generateBookList = async () => {
    const container = document.querySelector('main');
    const apiResponse = await fetch('/data/books.json');
    const booksInJson = await apiResponse.json();
    const bookList = document.createElement('tips-list');
    bookList.tips = booksInJson.books;
    container.innerHTML = '';
    container.appendChild(bookList);
};
