// ===========================
// Indicateur online/offline
// ===========================
const css = `
    <style>
      .led-box {
        width: 120px;
        height: 30px;
        margin: 0 2rem;
        display: flex;
        flex-direction: row;
        justify-content: flex-start;
      }
      .led-box span {
        line-height: 1.4rem;
        margin: 0 0.5rem
      }
      .led-green {
        margin: 0 auto;
        width: 24px;
        height: 24px;
        background-color: #abff00;
        border-radius: 50%;
        box-shadow: rgba(0, 0, 0, 0.2) 0 -1px 7px 1px, inset #304701 0 -1px 9px,
          #89ff00 0 2px 12px;
      }
      .led-red {
        margin: 0 auto;
        width: 24px;
        height: 24px;
        background-color: #f00;
        border-radius: 50%;
        box-shadow: rgba(0, 0, 0, 0.2) 0 -1px 7px 1px, inset #441313 0 -1px 9px,
          rgba(255, 0, 0, 0.5) 0 2px 12px;
      }
      .led-off {
        margin: 0 auto;
        width: 24px;
        height: 24px;
        background-color: #808080;
        border-radius: 50%;
        box-shadow: rgba(0, 0, 0, 0.2) 0 -1px 7px 1px, inset #403d3d 0 -1px 9px,
          white 0 2px 12px;
      }
    </style>
`;

const template = document.createElement('template');
template.innerHTML += `
    ${css}
    <div class="led-box">
      <div class="led-off"></div>
      <span></span>
    </div>
`;

class LedIndicatorElement extends HTMLElement {
    // define properties to store references to DOM elements in the component's template
    $led;
    $statusText;

    constructor() {
        super();
        this.attachShadow({ mode: 'open' });
        this.shadowRoot.appendChild(template.content.cloneNode(true));
        this.$led = this.shadowRoot.querySelector('div:first-child');
        this.$statusText = this.shadowRoot.querySelector('span');
        this.status = '---';
        this.render();
    }

    // components attributes
    static get observedAttributes() {
        return ['status'];
    }

    // connect component
    connectedCallback() {
        this.render();
    }

    // attribut change
    attributeChangedCallback(property, oldValue, newValue) {
        if (property === 'status' && oldValue !== newValue) {
            this[property] = newValue;
            this.render();
        }
    }

    render() {
        this.$statusText.textContent = `S.W. ${this.status}`;
        this.$led.setAttribute(
            'class',
            this.status === 'off'
                ? 'led-red'
                : this.status === 'on'
                ? 'led-green'
                : 'led-off',
        );
    }
}

export default LedIndicatorElement;
