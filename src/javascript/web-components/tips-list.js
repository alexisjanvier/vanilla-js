// =====================
// Affichage des listes
// =====================

const listItemTemplate = document.createElement('template');
listItemTemplate.innerHTML += `
    <style>
      @import url("https://cdn.jsdelivr.net/npm/@picocss/pico@1/css/pico.min.css")
    </style>
    <article>
      <hgroup>
        <slot name="title"></slot>
        <slot name="subtitle"></slot>
      </hgroup>
      <slot name="image"></slot>
    </article>
`;

export class ListItem extends HTMLElement {
    constructor() {
        super();
        this.attachShadow({ mode: 'open' });
        this.render();
    }

    render() {
        this.shadowRoot.appendChild(listItemTemplate.content.cloneNode(true));
    }
}

export class List extends HTMLElement {
    set tips(arrayOftips) {
        this.items = arrayOftips;
        this.renderList();
    }

    get tips() {
        return this.items || [];
    }

    constructor() {
        super();
        this.attachShadow({ mode: 'open' });
        this.items = [];
        this.renderList();
    }

    connectedCallback() {}

    renderList() {
        this.shadowRoot.innerHTML = `
      <style>
        @import url("https://cdn.jsdelivr.net/npm/@picocss/pico@1/css/pico.min.css")
      </style>
		  <section class="grid">
		    ${this.items
                .map(
                    (item) => `<article>
            <hgroup>
              <h2>${item.title}</h2>
              <h3>${item.subtitle}</h3>
            </hgroup>
            <img
              src="/images/${item.image}"
              alt="${item.title}"
            />
          </article>`,
                )
                .join('')}
		  </section>
		`;
    }
}
