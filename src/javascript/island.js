import LedIndicatorElement from './web-components/led-indicator.js';
import { List } from './web-components/tips-list.js';

import { initNavigation } from './navigation.js';
import { renderHome } from './island-beach.js';

customElements.define('tips-list', List);
customElements.define('led-indicator', LedIndicatorElement);

window.addEventListener('DOMContentLoaded', async () => {
    initNavigation();
    await renderHome();
});
