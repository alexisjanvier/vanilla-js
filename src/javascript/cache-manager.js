import { showResult } from './sw-manager.js';

export async function cacheMultipleFiles() {
    const cacheName = 'island-assets';
    if ('caches' in window) {
        try {
            const cache = await caches.open(cacheName);
            const urlsToCache = [
                '/',
                'livres.html',
                'disques.html',
                'css/custom.css',
                'javascript/island.js',
                'javascript/cache-manager.js',
                'javascript/list-manager.js',
                'javascript/sw-manager.js',
                'javascript/web-components/led-indicator.js',
                'javascript/web-components/tips-list.js',
                'https://cdn.jsdelivr.net/npm/@picocss/pico@1/css/pico.min.css',
            ];
            await cache.addAll(urlsToCache);
            showResult(
                `${urlsToCache.length} files were cached on ${cacheName}`,
            );
        } catch (error) {
            showResult(`Error while caching multiple files. ${error.message}`);
        }
    } else {
        showResult('Cache Storage not available');
    }
}

export async function deleteCache() {
    const cacheName = 'island-assets';
    if ('caches' in window) {
        await caches.delete(cacheName);
        showResult(`${cacheName} cache was deleted`);
    } else {
        showResult('Cache Storage not available');
    }
}
