import { deleteCache } from './cache-manager.js';

// Check a service worker registration status
export async function checkRegistration() {
    if ('serviceWorker' in navigator) {
        const registration = await navigator.serviceWorker.getRegistration();
        if (registration) {
            showResult('Le service worker est déjà démarré.', true);
        } else {
            showResult(
                "Aucun service worker n'est actuellement enregistré.",
                false,
            );
        }
    } else {
        showResult(
            "L'API service worker n'est pas disponible sur ce navigateur.",
            false,
        );
    }
}

// Registers a service worker
export async function register() {
    if ('serviceWorker' in navigator) {
        try {
            // Change the service worker URL to see what happens when the SW doesn't exist
            await navigator.serviceWorker.register('service-worker.js');
            showResult('Service worker activé \\o/', true);
        } catch (error) {
            showResult(`Erreur lors de l'activation: ${error.message}`, false);
        }
    } else {
        showResult(
            "L'API service worker n'est pas disponible sur ce navigateur.",
            false,
        );
    }
}

// Unregister a currently registered service worker
export async function unregister() {
    await deleteCache();
    if ('serviceWorker' in navigator) {
        try {
            const registration =
                await navigator.serviceWorker.getRegistration();
            if (registration) {
                const result = await registration.unregister();
                showResult(
                    result
                        ? 'Service worker désactivé'
                        : 'Le service worker ne peut pas être désactivé.',
                    result ? false : true,
                );
            } else {
                showResult("Il n'y a pas de service worker activé.", false);
            }
        } catch (error) {
            showResult(
                `Erreur lors de la désactivation: ${error.message}`,
                false,
            );
        }
    } else {
        showResult(
            "L'API service worker n'est pas disponible sur ce navigateur.",
            false,
        );
    }
}

export function showResult(text, connected = false) {
    const showContainer = document.querySelector('output');
    if (showContainer) {
        showContainer.innerHTML = text;
    }
    const ledStatus = document.querySelector('led-indicator');
    if (ledStatus) {
        ledStatus.setAttribute('status', connected ? 'on' : 'off');
    }
}
