import { generateBookList, generateMusicList } from './list-manager.js';
import { unregister, register, checkRegistration } from './sw-manager.js';

export const renderHome = async () => {
    const template = document.createElement('template');
    template.innerHTML = `
    <section>
        <article>
        <h1>Service Worker</h1>
        <p class="instructions">
            On peut vérifier dans les outils de développement du navigateur le
            statut du service worker !
        </p>
        <section class="toolbar grid">
            <button id="register" class="contrast outline">
            Activer le service worker
            </button>
            <button id="unregister" class="contrast outline">
            Désactiver le service worker
            </button>
        </section>
        <output></output>
        </article>
    </section>`;
    const clone = template.content.cloneNode(true);
    const container = document.querySelector('main');
    container.innerHTML = '';
    container.appendChild(clone);
    checkRegistration();
    const registerButton = document.getElementById('register');
    if (registerButton) {
        registerButton.addEventListener('click', register);
    }
    const unregisterButton = document.getElementById('unregister');
    if (unregisterButton) {
        unregisterButton.addEventListener('click', unregister);
    }
    const bookListContainer = document.getElementById('book-list');
    if (bookListContainer) {
        await generateBookList('book-list');
    }
    const musicListContainer = document.getElementById('music-list');
    if (musicListContainer) {
        await generateMusicList('music-list');
    }
};
