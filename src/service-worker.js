const urlsToCache = [
	"/",
	"favicon.ico",
	"css/custom.css",
	"javascript/island.js",
	"javascript/navigation.js",
	"javascript/cache-manager.js",
	"javascript/island-beach.js",
	"javascript/list-manager.js",
	"javascript/sw-manager.js",
	"javascript/web-components/led-indicator.js",
	"javascript/web-components/tips-list.js",
	"data/albums.json",
	"data/books.json",
	"https://cdn.jsdelivr.net/npm/@picocss/pico@1/css/pico.min.css",
];

self.addEventListener("install", async (event) => {
	const cache = await caches.open("island-assets");
	// it stores all resources on first SW install
	cache.addAll(urlsToCache);
});

self.addEventListener("fetch", (event) => {
	event.respondWith(
		caches.match(event.request).then((cachedResponse) => {
			const networkFetch = fetch(event.request)
				.then((response) => {
					// update the cache with a clone of the network response
					return caches.open("island-assets").then((cache) => {
						cache.put(event.request, response.clone());
						return response;
					});
				})
				.catch((error) => {
					console.log(`Probleme lors de la mise en cache de ${event.request} : ${error.message}`);
				});
			// prioritize cached response over network
			return cachedResponse || networkFetch;
		}),
	);
});
