/** @type {import('vite').UserConfig} */
export default {
	root: "src/",
	server: {
		port: 8080,
	},
};
